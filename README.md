#  Add selected Photos (from Picker) to a Map

Sample App that demonstrates some of the techniques to put photos on a Map and get them refreshed.

Specifically using `MKMapView` because the SwiftUI `Map` still has no satellite image in 2022! :(

It also demonstrates how to center the map to the User location as well as use `UserTrackingButton` in SwiftUI.
