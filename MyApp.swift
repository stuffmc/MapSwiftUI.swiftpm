import SwiftUI
import PhotosUI
import MapKit

struct ImagePicker: UIViewControllerRepresentable {
    let coordinator = Coordinator()
    let picker = UIImagePickerController()

    func makeUIViewController(context: Context) -> UIImagePickerController {
        coordinator.owner = self
        coordinator.dismiss = {
            picker.dismiss(animated: true)
        }
        picker.sourceType = .photoLibrary
        picker.delegate = context.coordinator
        return picker
    }

    func makeCoordinator() -> Coordinator {
        coordinator
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) { }
}

class Coordinator: NSObject, ObservableObject, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    @Published var photos: [Photo] = []
    var owner: ImagePicker?
    var dismiss: (() -> Void)?

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let coordinate = CLLocationCoordinate2D(latitude: CGFloat.random(in: 37.32...37.33),
                                                longitude: -CGFloat.random(in: 122.03...122.04))
        if let image = info[.originalImage] as? UIImage {
            photos.append(Photo(coordinate: coordinate, image: image))
        }
        dismiss?()
    }

}

class Photo: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var image: UIImage

    init(coordinate: CLLocationCoordinate2D, image: UIImage) {
        self.coordinate = coordinate
        self.image = image
    }
}

struct UserTrackingButton: UIViewRepresentable {
    let mapView: MapView

    func makeUIView(context: Context) -> MKUserTrackingButton {
        MKUserTrackingButton(mapView: mapView.mapView)
    }

    func updateUIView(_ uiView: MKUserTrackingButton, context: Context) { }
}

struct MapView: UIViewRepresentable {
    let mapView = MKMapView()
    @Binding var mapType: Int
    @ObservedObject var coordinator: Coordinator
    var action: (Image) -> Void

    func makeUIView(context: Context) -> MKMapView {
        mapView.delegate = context.coordinator
        mapView.mapType = MKMapType(rawValue: UInt(mapType))!
        return mapView
    }

    func makeCoordinator() -> MapViewDelegate {
        MapViewDelegate(self)
    }

    func updateUIView(_ uiView: MKMapView, context: Context) {
        mapView.addAnnotations(coordinator.photos)
    }

    class MapViewDelegate: NSObject, MKMapViewDelegate, CLLocationManagerDelegate {
        private var userLocationUpdated = false
        private var locationManager = CLLocationManager()

        var owner: MapView

        init(_ owner: MapView) {
            self.owner = owner
            super.init()
            locationManager.delegate = self
            locationManager.requestLocation()
        }

        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard let annotation = annotation as? Photo else { return nil }
            let size = CGSize(width: 50, height: 50)
            let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            let view = MKAnnotationView(frame: rect)
            view.clipsToBounds = true
            var inner = size.height - size.width * 1/4
            let rectangle = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: inner))
            rectangle.addSubview(UIImageView(image: UIGraphicsImageRenderer(size: size).image { _ in
                                annotation.image.draw(in: CGRect(origin: .zero, size: size))
                            }))
            rectangle.layer.cornerRadius = 5
            rectangle.layer.borderColor = UIColor.white.cgColor
            rectangle.layer.borderWidth = 3
            rectangle.clipsToBounds = true
            view.addSubview(rectangle)

            let shape = CAShapeLayer()
            let path = CGMutablePath()

            // Arrow
            inner -= 3
            let point = CGPoint(x: size.width * 3/4, y: inner)
            path.move(to: point)
            path.addLine(to: CGPoint(x: size.width / 2, y: size.height))
            path.addLine(to: CGPoint(x: size.width * 1/4, y: inner))
            path.addLine(to: point)
            shape.fillColor = rectangle.layer.borderColor
            shape.path = path
            shape.cornerRadius = 10
            view.layer.addSublayer(shape)
            return view
        }

        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            if !userLocationUpdated {
                owner.mapView.userTrackingMode = .followWithHeading
                userLocationUpdated.toggle()
            }
        }

        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            guard let annotation = view.annotation as? Photo else { return  }
            owner.action(Image(uiImage: annotation.image))
        }

        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if !userLocationUpdated {
                owner.mapView.userTrackingMode = .followWithHeading
                userLocationUpdated.toggle()
            }
        }

        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print(error)
        }
    }
}

@main
struct MyApp: App {
    private let imagePicker = ImagePicker()
    @State private var mapView: MapView?
    private let locationManager = CLLocationManager()
    @State private var isPresented = false
    @State private var image: Image?
    @State var mapType = 0

    var body: some Scene {
        WindowGroup {
            VStack {
                if let mapView = mapView {
                    HStack {
                        UserTrackingButton(mapView: mapView).frame(width: 40, height: 40)
                            .mask(Circle())
                        Button {
                            isPresented = true
                        } label: {
                            Label("Pick", systemImage: "photo.circle.fill")
                        }
                        ZStack(alignment: .leading) {
                            RoundedRectangle(cornerRadius: 10)
                                .foregroundColor(.white)
                                .padding([.top, .leading], 1)
                            Picker(selection: $mapType, label: EmptyView()) {
                                Text("Satellite").tag(Int(MKMapType.hybrid.rawValue))
                                Text("Standard").tag(Int(MKMapType.standard.rawValue))
                            }
                            .pickerStyle(SegmentedPickerStyle())
                        }
                        .frame(width: 150, height: 25)
                    }
                    mapView
                }
            }
            .sheet(isPresented: .constant(image != nil), onDismiss: {
                image = nil
            }, content: {
                image
            })
            .sheet(isPresented: $isPresented) { imagePicker }
            .onAppear {
                locationManager.requestWhenInUseAuthorization()
                mapView = MapView(mapType: $mapType, coordinator: imagePicker.coordinator) {
                    image = $0
                }
            }
        }
    }
}
